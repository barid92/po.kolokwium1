﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Hierarchia jest zapisana w klasach, tylko pierwsze sie nie zgadza
            A a = new A();
            J j = new J();
            J e = new D();
            K h = new H();
            K d = new D();
            B b = new B();
            B c = new C();

            Console.WriteLine(e is H);
            Console.WriteLine(c is A);
            Console.WriteLine(j is K);
            Console.WriteLine(d is B);
            Console.WriteLine(c is J);
            Console.WriteLine(a is B);

            Console.ReadKey();

        }
    }
}
