﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            var myBoolTab = new bool[3];
            myBoolTab[0] = true;
            myBoolTab[1] = false;
            myBoolTab[2] = false;
            Console.WriteLine("Tablica ");

            foreach (var item in myBoolTab)
            {
                Console.Write(item);
            }
            Console.WriteLine();
            Console.WriteLine("Ile true");
            Console.WriteLine(myBoolTab.IleTrue());
            Console.WriteLine("Ile true po negacji");
            myBoolTab.Negacja();
            Console.WriteLine(myBoolTab.IleTrue());
            myBoolTab.Negacja();
            Console.WriteLine("Tablica w odwrotnej kolejnosci");

            foreach (var item in myBoolTab.Odwroc())
            {
                Console.Write(item);
            }
            Console.ReadKey();
        }
    }
}
