﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    static class BoolTab
    {
        public static int IleTrue(this bool[] myTab)
        {
            int myInt = 0;
            foreach (var item in myTab)
            {
                if (item)
                {
                    myInt++;
                }
            }
            return myInt;
        }
        public static void Negacja(this bool[] myTab)
        {
            for (int i = 0; i < myTab.Length ; i++)
            {
                if (myTab[i])
                {
                    myTab[i] = false;
                }
                else
                {
                    myTab[i] = true;
                }
            }
        }
        public static bool[] Odwroc(this bool[] myTab)
        {
            var myNewTab = new bool[myTab.Length];
            int y = 0;
            for (int i = myTab.Length -1; i >= 0; i--)
            {
                myNewTab[y] = myTab[i];
                y++;
            }
            return myNewTab;
        }
    }
}
