﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {

            var lista = new List<UrzadzeniaAGD>();
            lista.Add(new Blender(2011, 5555));
            lista.Add(new Mikser(2015, 789));
            foreach (var item in lista)
            {
                Console.WriteLine(item);
                Console.WriteLine("To jest " + item.Typ);
                item.Wlacz();
                Console.WriteLine(item.DaneUrzadzenia());
                item.Dzwiek();
                
            }
            Console.ReadKey();
        }
    }
}
