﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    abstract class UrzadzeniaAGD
    {
        protected int rokProdukcji;
        protected int moc;
        public UrzadzeniaAGD(int rokProdukcji)
        {
            this.rokProdukcji = rokProdukcji;
        }
        public UrzadzeniaAGD(int rokProdukcji,int moc)
        {
            this.rokProdukcji = rokProdukcji;
            this.moc = moc;
        }
        public abstract string DaneUrzadzenia();
        public abstract void Dzwiek();
        public abstract void Wlacz();
        public abstract string Typ {get;}
    }
}
