﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Mikser : UrzadzeniaAGD
    {
        public Mikser(int rokProdukcji)
            : base(rokProdukcji)
        {
        }
        public Mikser(int rokProdukcji, int moc)
            : base(rokProdukcji, moc)
        {
        }
        public override string DaneUrzadzenia()
        {
            return "To jest mikser";
        }
        public override void Dzwiek()
        {
            Console.WriteLine("bzium bzium bzium");
        }
        public override string Typ
        {
            get { return "Mikser"; }
        }
        public override void Wlacz()
        {
            Console.WriteLine("Wlaczono Mikser");
        }
        public override string ToString()
        {
            return String.Format("Mikser o mocy {0} wyprodukowany w roku {1}",moc,rokProdukcji);
        }
    }
}
