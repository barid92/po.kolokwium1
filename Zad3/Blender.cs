﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Blender : UrzadzeniaAGD
    {
        public Blender(int rokProdukcji)
            : base(rokProdukcji)
        {
        }
        public Blender(int rokProdukcji, int moc)
            : base(rokProdukcji, moc)
        {
        }
        public override string DaneUrzadzenia()
        {
            return "To jest blender";
        }
        public override void Dzwiek()
        {
            Console.WriteLine("bzzzz bzzzz bzzzz");
        }
        public override string Typ
        {
            get { return "Blender"; }
        }
        public override void Wlacz()
        {
            Console.WriteLine("Wlaczono Blender");
        }
        public override string ToString()
        {
            return String.Format("Blender o mocy {0} wyprodukowany w roku {1}", moc, rokProdukcji);
        }
    }
}
