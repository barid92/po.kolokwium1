﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            var myBestString = new LepszyString("aba");
            //myBestString.Podwoj();
            Console.WriteLine(myBestString);
            Console.WriteLine(myBestString.IleSamoglosek());
            myBestString.Polowa();
            Console.WriteLine(myBestString);
            Console.WriteLine("Zapianie do pliku");
            myBestString.ZapiszDoPliku("plik.txt");
            Console.WriteLine("Odczyt z pliku pliku");
            var LepszyStringZPliku = LepszyString.WczytajZPliku("plik.txt");
            Console.WriteLine(LepszyStringZPliku);

            Console.ReadKey();
        }
    }
}
