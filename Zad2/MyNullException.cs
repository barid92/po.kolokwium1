﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class MyNullException : Exception
    {
        public MyNullException() : base()
        {
            throw new Exception("Wartosc jest null");
        }
    }
}
