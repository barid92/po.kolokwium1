﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class LepszyString
    {
        private StringBuilder sb;
        public LepszyString(string tekst)
        {
            sb = new StringBuilder();
            sb.Append(tekst);
        }
        public void Podwoj()
        {
            var myTemp = sb.ToString();
            sb.Clear();
            foreach (char item in myTemp)
            {
                sb.Append(item);
                sb.Append(item);
            }
        }
        public override string ToString()
        {
            return sb.ToString();
        }
        public int IleSamoglosek()
        {
            var listaSamoglosek = new List<char>(){'a','e','i','o','u'};
            int liczbaSamoglosek = 0;
            foreach (var literaSB in sb.ToString())
            {
                foreach (var samogloska in listaSamoglosek)
                {
                    if (literaSB == samogloska)
                    {
                        liczbaSamoglosek++;
                        break;
                    }
                }
            }
            return liczbaSamoglosek;
        }
        public void Polowa()
        {
            int mojaLiczba = 0;
            string liczbaZnakow = (Convert.ToDouble(sb.ToString().Length) / 2).ToString();
            if (liczbaZnakow.IndexOf(',') != -1)
            {
                liczbaZnakow = (Convert.ToInt32(liczbaZnakow.Split(',')[0]) + 1).ToString();
            }
            mojaLiczba = Convert.ToInt32(liczbaZnakow);
            var myTemp = sb.ToString();
            sb.Clear();
            sb.Append(myTemp.Substring(0,mojaLiczba));

        }
        public static void KtoDluzszy(LepszyString mystringone, LepszyString mystringtwo)
        {
            if (mystringone == null || mystringtwo == null)
            {
                throw new MyNullException();
            }
            int myStringOneCount = mystringone.ToString().Length;
            int myStringTwoCount = mystringtwo.ToString().Length;
            if (myStringOneCount > myStringTwoCount)
            {
                Console.WriteLine(String.Format("Wartosc pierwsza jest wieksza o {0}",(myStringOneCount- myStringTwoCount)));
            }
            else if (myStringOneCount < myStringTwoCount)
	        {
                Console.WriteLine(String.Format("Wartosc druga jest wieksza o {0}", (myStringTwoCount - myStringOneCount)));
	        }
            else
            {
                Console.WriteLine(String.Format("Wartosci sa sobie rowne"));
            }
        }
        public void ZapiszDoPliku(string nazwaPliku)
        {
            using (StreamWriter sw = new StreamWriter(nazwaPliku))
            {
                sw.WriteLine(sb.ToString());
                sw.Close();
            }
        }
        public static LepszyString WczytajZPliku(string nazwaPliku)
        {
            using (StreamReader sr = new StreamReader(nazwaPliku))
            {
                return new LepszyString(sr.ReadLine());
            }
        }
    }
}
